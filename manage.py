#!/usr/bin/env python
import os

from flask_script import Manager, Shell
from app import create_app as application


application = create_app(os.getenv('FLASK_CONFIG') or 'local')

manager = Manager(application)

def _make_shell_context():
    return dict(app=application)

manager.add_command('shell', Shell(make_context=_make_shell_context))

if __name__ == '__main__':
    manager.run()