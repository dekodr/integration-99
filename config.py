import os, json

class Config:
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    DEBUG = True

    SOURCE = '99RESTAURANT'
    URL = os.getenv('URL')
    USERNAME = os.getenv('USERNAME')
    PASSWORD = os.getenv('PASS')

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,  # If you declare global level logger.
        'formatters': {
            'json': {
                'class': 'pythonjsonlogger.jsonlogger.JsonFormatter',
                'format': '%(asctime)s %(msecs)d %(process)d %(threadName)s %(name)s %(levelname)s %(filename)s %(lineno)s %(message)s',
                'datefmt': '%Y-%m-%dT%H:%M:%S%z'
            }
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'json',
                'stream': 'ext://sys.stdout'
            }
        },
        'root': {  # this logger is for external library.
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': False
        },
        'loggers': {
            'app': {
                'handlers': ['console'],
                'level': 'DEBUG',
                'propagate': False
            }
        }
    }

class TestingConfig(Config):
    pass

class LocalConfig(Config):
    DEBUG = True

class StagingConfig(Config):
    DEBUG = False

class ProductionConfig(Config):
    DEBUG = True

config = {
    'local': LocalConfig,
    'testing': TestingConfig,
    'staging': StagingConfig,
    'production': ProductionConfig
}