import logging.config, json
from flask_api import FlaskAPI
from config import config

def create_app(config_name):
    application = FlaskAPI(__name__)
    application.config.from_object(config[config_name])
    logging.config.dictConfig(application.config['LOGGING'])

    from .status import status
    from .transaction import transaction

    application.register_blueprint(status)
    application.register_blueprint(transaction)

    return application