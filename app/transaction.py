from flask import Blueprint, request, current_app, jsonify, abort
from marshmallow import Schema, ValidationError, fields
from .common.ninetynine import NinetyNine, col
import datetime, requests, json, sys, base64, os, time
from dateutil import tz
import pytz
from pytz import timezone
transaction = Blueprint('transactions', __name__)

class TransactionSchema(Schema):
    receipt_date = fields.Str(
        required=True,
        error_messages={'required': 'Receipt Date is Required'}
    )
    pos = fields.Str(
        required=True,
        error_messages={'required': 'POS is Required'}
    )
    tax = fields.Str(
        required=True,
        error_messages={'required': 'Tax is Required'}
    )
    total = fields.Int(
        required=True,
        error_messages={'required': 'Total Amount is Required'}
    )
    store = fields.Int(
        required=True,
        error_messages={'required': 'ID Store is Required'}
    )
    detail = fields.Nested('TransactionDetailSchema', default=[], many=True)

class TransactionDetailSchema(Schema):
    product = fields.Str( 
        required=True,
        error_messages={'required': 'Product is Required'}
    )
    quantity = fields.Int(
        required=True,
        error_messages={'required': 'Quantity is Required'}
    ) 
    price = fields.Int(
        required=True,
        error_messages={'required': 'Price is Required'}
    )

@transaction.route('/transactions', methods=['POST'])  
def transactions():  
    # if not request.json :
    #     return {'result': 'Request body must be in JSON Format'}, 500
    os.environ['TZ'] = 'Asia/Jakarta'
    data = request.get_data(as_text=True)
    forms = request.form
    raw_data = base64.b64decode(forms['data'])
    id_cashier = forms['id'][-6:].lstrip('0')

    # payload = request.get_json() or {}
    # try:
    #     data, _ = TransactionSchema(strict=True).load(payload)
    # except ValidationError as e:
    #     return jsonify(e.messages), 422

    # current_app.logger.debug(dict(
    #     id_cashier=id_cashier
    # ))
    ninetynine = NinetyNine()
    data_filter = ninetynine.extract(data=raw_data.decode('utf-8'))
    if data_filter != False: 
        r = requests.get('http://172.30.0.3:8990/api/cashier/'+str(id_cashier))
        data_cashier = r.json() 
        headers = {'content-type':'application/json'}
        lat = -6.2257529 if (data_cashier['store_id']==5) else -6.2240072 if(data_cashier['store_id']==4) else -6.1950298 if(data_cashier['store_id']==3) else -6.1883857
        lon = 106.8233048 if (data_cashier['store_id']==5) else 106.8209864 if(data_cashier['store_id']==4) else 106.821442 if(data_cashier['store_id']==3) else -6.1883857
        data_cashier['lat'] = lat
        data_cashier['lon'] = lon
        data_cashier['zip_code'] = 12710 if (data_cashier['store_id']==5) else 12940 if(data_cashier['store_id']==4) else 10310 if(data_cashier['store_id']==3) else 11610
        store_id = data_cashier['terminal_id'][-11:-6].lstrip("0")
        tr_master = { 
            "merchant_id": 1,
            "store_id":store_id,
            "cashier_id":id_cashier,
            "category_id":"1",
            "meta": data_filter['meta'],
            "receipt_date":data_filter['receipt_date'], 
            "total":data_filter['total'],
            "tax":data_filter['tax'],
            "detail":data_filter['detail'] 
        }

        # tr_req = requests.post('http://172.30.0.3:8990/api/transaction', data=json.dumps(tr_master),headers=headers)    
        
        # current_app.logger.debug(dict(
        #     tr_req=tr_req.content
        # ))
        # data_filter['receipt_date'] =
        if headers :  
            now = datetime.datetime.now() 
            time_format = "%Y-%m-%d %H:%M:%S"
            # prin
            utcmoment = now.replace(tzinfo=pytz.utc)
            # localDatetime = utcmoment.astimezone(pytz.timezone('Asia/Jakarta'))
            now = utcmoment.strftime(time_format)
            receipt_date = data_filter['receipt_date']+'+0700'
            receipt_date = datetime.datetime.strptime(receipt_date,time_format+'%z')
            receipt_date = receipt_date.strftime('%Y-%m-%d %H:%M:%S')
            sekarang = datetime.datetime.now()
            utc = pytz.timezone('UTC')
            aware_date = utc.localize(sekarang)
            aware_date.tzinfo # <UTC>
            aware_date.strftime("%a %b %d %H:%M:%S %Y")
            tojkt = pytz.timezone('Asia/Jakarta')
            rawjkt = aware_date.astimezone(tojkt)
            rawjkt.tzinfo
            waktujkt = rawjkt.strftime("%Y-%m-%d %H:%M:%S")
            if data_filter['meta']['closed']=="-":
                closed = waktujkt
                a = datetime.datetime.strptime(receipt_date,"%Y-%m-%d %H:%M:%S")
                b = datetime.datetime.strptime(closed,"%Y-%m-%d %H:%M:%S")
                datediff =(b-a)
                if datediff.days >= 1:
                    closed = a + datetime.timedelta(hours=2)
                    closed = closed.strftime('%Y-%m-%d %H:%M:%S')
                else:
                    if (datediff.seconds/3600) > 10:
                        closed = a + datetime.timedelta(hours=2)
                        closed = closed.strftime('%Y-%m-%d %H:%M:%S')
            else:
                closed = data_filter['meta']['closed']+'+0700'
                closed = datetime.datetime.strptime(closed,time_format+'%z')
                closed = closed.strftime('%Y-%m-%d %H:%M:%S')
            transfeed = 'transactionfeed_99_'+rawjkt.strftime('%Y%m')
            # url = "https://elastic:jKL5FDeoiASmwiYr9JNjkXsr@7e6694cc44e846b5932b240d97959562.ap-southeast-1.aws.found.io:9243/"+transfeed+"/_doc/"
            url = "https://925dig0464:bfwh859hu0@dam-es-1149805810.ap-southeast-2.bonsaisearch.net:443/"+transfeed+"/_doc/"
            # url = "https://925dig0464:bfwh859hu0@dam-es-1149805810.ap-southeast-2.bonsaisearch.net:443/_doc/"
            data = {
                    "merchant" :{
                        "id" : data_cashier['merchant_id'],
                        "name" :  data_cashier['merchant_name']
                    },
                    "table" : data_filter['meta']['table'],
                    "pax" : data_filter['meta']['pax'],
                    "entry_time" :{
                        "open" : receipt_date,
                        "closed" : closed
                    },
                    "server" : {
                        "id" : "",
                        "name" : data_filter['meta']['server']
                    },
                    "customer" :{
                        "id" :"",
                        "name" : "",
                        "email" : "",
                        "member_type" : "3",
                        "phone_number" : "",
                        "remaining_balance_member" : 0,
                        "loyalty_point_member" : 0
                    },
                    "receipt" : {
                        "date" : closed,
                        "invoice_number" : data_filter['meta']['invoice'],
                        "items":data_filter['detail'],
                        # "items" : [{
                        #     "sku" : "D01",
                        #     "product_name" : "tequila",
                        #     "quantity" : "2",
                        #     "price" : 1200000,
                        #     "total_price" : 2400000,
                        #     "discount" : 0
                        # },
                        # {
                        #     "sku" : "D02",
                        #     "product_name" : "Bir Bintang 500ml",
                        #     "quantity" : "2",
                        #     "price" : 50000,
                        #     "total_price" : 100000,
                        #     "discount" : 20000
                        # }],
                        "total_items" : len(data_filter['detail']),
                        "currency" : "Rupiah",
                        "subtotal" : data_filter['subtotal'],
                        "percent_disc" : data_filter['meta']['percent_disc'],
                        "discount" : data_filter['meta']['discount'],
                        "tax" :data_filter['tax'],
                        "service_charge" : data_filter['meta']['service_charge'],
                        "delivery_service_charge" : data_filter['meta']['delivery_service_charge'],
                        "total" : data_filter['total'],
                        "change" : data_filter['meta']['change']
                    },
                    "payment" : {
                        "reference_number" : "",
                        "method" : data_filter['meta']['method'],
                        "sof_wallet" : data_filter['meta']['sof_wallet'],
                        "card" : {
                            "card_type" : data_filter['meta']['issuer'],
                            "BIN" : "",
                            "acquirer" : "",
                            "issuer" : data_filter['meta']['issuer'],
                            "name" : "",
                            "on_us" : "N"
                        },
                        "approval_code" : "",
                        "promo_code" : "",
                        "voucher_code" : "",
                        "edc" :
                        {
                            "tid" : "",
                            "device_id" : "",
                            "device_user" : "",
                            "app_name" : "",
                            "app_version" : ""
                        }
                    },
                    "pos" : {
                        "id" : data_cashier['terminal_id'],
                        "name" : data_filter['meta']['pos'],
                        "cashier" :
                        {
                            "id" : data_cashier['cashier_id'],
                            "name" : data_filter['meta']['cashier']
                        },
                        "app_name" : data_filter['meta']['pos'],
                        "version" : "1.1"
                    },
                    "store" :{
                        "id" : data_cashier['store_id'],
                        "name" : data_cashier['store_name'],
                        "province" : data_cashier['province_name'],
                        "city" : data_cashier['city_name'],
                        "address" : data_cashier['address'],
                        "zip_code" :data_cashier['zip_code'],
                        "location" :
                        {
                            "lat" : data_cashier['lat'],
                            "lon" : data_cashier['lon']
                        }
                    },
                    "store_manager" : "",
                    "transaction" : {
                        "type" : data_filter['meta']['transaction_type'],
                        "provider" : data_filter['meta']['delivery_provider']
                    },
                    "date_entry" : waktujkt
            }
            # return data, 200
            # { 
            #     "entry_stamp": now.strftime("%m/%d/%Y, %H:%M:%S"), 
            #     "id": result, 
            #     "id_elk": "", 
            #     "item": data_filter['detail'], 
            #     "payment_method": "", 
            #     "pos": id_cashier, 
            #     "raw_bottom": data_filter['meta'], 
            #     "receipt_date": data_filter['receipt_date'], 
            #     "store_name": data_cashier['store_name'], 
            #     "total": data_filter['total'],
            #     "device": "pos",
            #     "deviceid": data_cashier['terminal_id'],
            #     "appname": "smartcart",
            #     "appversion": "1.0"
            # }
            # return data, 200
            current_app.logger.info(dict(
                message="=== Data to JASK ===",
                body=data
            ))
            headers = {'Content-type': 'application/json'}
            res = requests.post(url,  headers=headers, json=data)
            data_result = res.json() 
            current_app.logger.info(dict(
                url=url,
                headers=headers,
                message="=== Request JASK ===",
                body=data
            ))
            detail = []
            items = []
            transfeeditem = 'transaction_receipt_detail_99_'+rawjkt.strftime('%Y%m')
            urlitem = "https://925dig0464:bfwh859hu0@dam-es-1149805810.ap-southeast-2.bonsaisearch.net:443/"+transfeeditem+"/_doc/"
            for product in data_filter['detail']:
                new_data = copy.deepcopy(data)
                new_data['receipt']['items'] = product
                post_item = requests.post(urlitem,  headers=headers, json=new_data)
                items.append(post_item.json())
        #     if data_result['pn-status']=='success.': 
        #         try:
        #             _upd = col.update_one({'_id':result},{'$set': {'jask_data': data_result}} )
        # return {'result': "Successful Insert Data"}, 200
        # return {'result':data}, 200
        return {'result':data_result, 'items':items, 'sent':data}, res.status_code
        #         except pymongo.errors.OperationFailure as err:  
        #             return {'result': 'Fail', 'Message':err}, 500 
        #     else: 
        #         return {'result': 'Fail', 'Message':data_result['message']}, 500 
        # else:
        #     return {'result': 'Fail'}, 500
    else:
        return {'result': 'Fail', 'Message':'Not a valid invoice format'}, 500
