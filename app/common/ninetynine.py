from flask import current_app
from datetime import datetime
from requests.exceptions import RequestException
from json import JSONDecodeError
from bson import json_util
import requests, os, re, math
from pytz import timezone
import base64
import json
import datetime
from pymongo import MongoClient
user_db = os.environ['MONGO_USER']
pass_db = os.environ['MONGO_PASS']

client = MongoClient('172.30.0.4', 27017) # host=['mongo_99:27017'], username=user_db, password=pass_db, authSource='smartcartdb', authMechanism='SCRAM-SHA-256'
client.smartcartdb.authenticate(user_db, pass_db, mechanism='SCRAM-SHA-256')
db = client.smartcartdb
col = db.transaction
class NinetyNine: 
    def __init__(self):
        now = datetime.datetime.now()
        self.source = current_app.config.get('SOURCE')
        self.main_url = current_app.config.get('URL')
        self.username = current_app.config.get('USERNAME')
        self.password = current_app.config.get('PASSWORD')
    
    def transaction(self, data):
        res = col.insert_one(data).inserted_id
        return res
    def extract(self, data):
        y = data.replace("\x00","")
        y = y.replace("\n","|")
        y = re.sub('\s+', ' ', y).strip()
        match = re.search(r"Invoice",y) 
        sum_subtotal = 0
        sum_total = 0
        sum_discount = 0
        sum_tax = 0
        sum_service = 0 
        if match:
            # return y
            y = re.sub(r"==========================================",'',y)
            timestamp = re.search(r"([A-Z]{1}[a-z]{2}\s[0-9]{1,2}\,\s[0-9]{4}\s[0-9]{1,2}\:[0-9]{1,2}\:[0-9]{1,2}\s[A-Z]{2})",y).groups()
            timestamp = ''.join(timestamp)
            closed = re.search(r"Closed ([A-Z]{1}[a-z]{2}\s[0-9]{1,2}\,\s[0-9]{4}\s[0-9]{1,2}\:[0-9]{1,2}\:[0-9]{1,2}\s[A-Z]{2})",y)
            if closed:
                closed = ''.join(closed.groups())
                closed = datetime.datetime.strptime(closed, '%b %d, %Y %I:%M:%S %p')
                closed = timezone('Asia/Jakarta').localize(closed).strftime("%Y-%m-%d %H:%M:%S")
            else:
                closed = '-'
            date_time_obj = datetime.datetime.strptime(timestamp, '%b %d, %Y %I:%M:%S %p')
            date_time_obj = timezone('Asia/Jakarta').localize(date_time_obj).strftime("%Y-%m-%d %H:%M:%S")

            cashier = re.search(r"Cashier:\s([a-zA-Z]+)",y)
            if cashier:
                cashier = cashier.groups()[0]

            server = re.search(r"Server:\s([a-zA-Z\s]+)",y)
            if server:
                server = server.groups()[0]
            meta = {}
            invoice = re.search(r"Invoice\s([#a-zA-Z0-9]+)",y)
            pax = re.search(r"PAX:\s([a-zA-Z0-9]+)",y)
            table = re.search(r"(?!.*(?i)TBL TA|.*(?i)TBL WTA).*(?i)TBL\s([a-zA-Z0-9\s]+)",y)
            meta['delivery_provider'] = ""
            meta['sof_wallet'] = ""
            if table:
                table = table.groups()[0]
                meta['table'] = table 
                transaction_type = "DINE IN"
            else:
                table = re.search(r"(?i)TAKE AWAY|TAKEAWAY",y)
                meta['table'] = ""
                if table:
                    transaction_type = "TAKE AWAY"
                else:
                    table = re.search(r"(?i)TBL WTA|TBL TA",y)
                    if table:
                        transaction_type = "TAKE AWAY"
                    else:
                        transaction_type = "DELIVERY"
                        provider = re.search(r"(?i)GOJEK",y)
                        if provider:
                            provider = "GOJEK"
                        else:
                            provider = re.search(r"(?i)GO PAY",y)
                            if provider:
                                provider = "GOJEK"
                            else:
                                provider = "GRAB"
                        meta['delivery_service'] = "0"
                        meta['delivery_provider'] = provider
                        delivery_service = re.search(r"(?i)DELIVERY SERVICE\S([0-9\,]+)", y)
                        if delivery_service:
                            meta["delivery_service"] = delivery_service.groups()[0]
            get_product = re.findall(r"([0-9]+)\s([a-zA-Z0-9\s\.\,()\%\&\@\!\*\-]+)\s([0-9,.]+,[0-9,.]+)", y)
            data = {}
            meta['pax'] = pax.groups()[0]
            meta['cashier'] = cashier
            meta['transaction_type'] = transaction_type
            meta['invoice'] = invoice.groups()[0]
            meta['server'] = server
            meta['pos'] = re.search(r"POS:\s([a-zA-Z0-9]+)",y).groups()[0]
            meta['print_count'] = re.search(r"Print Cnt:([0-9]+)",y).groups()[0]
            meta['closed'] = closed
            detail = []
            subtotal = 0
            for product in get_product:
                data_detail = {}
                data_detail['sku'] = ""
                data_detail['product_name'] = product[1]
                data_detail['quantity'] = int(product[0])
                data_detail['price'] = int(product[2].replace(",",""))/int(product[0])
                data_detail['total_price'] = int(product[2].replace(",",""))
                data_detail['discount'] = 0
                subtotal += data_detail['total_price']
                detail.append(data_detail)
            data['detail'] = detail
            total = re.search(r"Total\s([0-9\,]+)",y)
            delivery_service_charge = 0
            service = re.search(r"(?i)SERVICE CHARGE\s([0-9\,]+)",y)
            if service:
                service = int((service.groups()[0]).replace(",",""))
            else:
                service = 0
                if transaction_type != "DELIVERY":
                    if transaction_type != "TAKE AWAY":
                        service = int(subtotal*7.5/100)
            meta['service_charge'] = service
            tax = re.search(r"(?i)Tax\s([0-9\,]+)",y)
            if tax:
                tax = tax.groups()[0]
                if transaction_type == "DELIVERY":
                    delivery = re.search(r"(?i)DELIVERY SERVICE[\s]+([0-9\,]+)",y)
                    if delivery:
                        delivery_service_charge = int(delivery.groups()[0].replace(",",""))
                    else:
                        delivery_service_charge = int(subtotal*10/100)
            else:
                tax = "0"
                if transaction_type != "DELIVERY":
                    if transaction_type != "TAKE AWAY":
                        tax = str(int(math.ceil((subtotal+service)*10/100)))
                    else:
                        tax = "0"
                else:
                    delivery = re.search(r"(?i)DELIVERY SERVICE[\s]+([0-9\,]+)",y)
                    if delivery:
                        delivery_service_charge = delivery.groups()[0]
                        tax = str(int(math.ceil((subtotal+delivery_service_charge)*10/100)))
                    else:
                        delivery_service_charge = int(subtotal*10/100)
                        tax = str(int(math.ceil((subtotal+delivery_service_charge)*10/100)))
            discount = re.search(r"(?i)([0-9]+)%\s-([0-9\,]+)",y)
            if discount:
                percent_disc = int(discount.groups()[0])/100
                discount = int(discount.groups()[1].replace(",",""))
            else:
                discount = 0
                percent_disc = 0
            meta['delivery_service_charge'] = delivery_service_charge
            meta['discount'] = discount
            meta['percent_disc'] = percent_disc
            meta['issuer'] = ""
            meta['payment'] = ""
            meta['method'] = "CASH"
            payment = re.search(r"(?i)(CASH)[\s]+([0-9,.]+)",y)
            if payment:
                meta['method'] = "CASH"
                meta['payment'] = payment.groups()[0]
            else:
                payment = re.search(r"(?i)(GO PAY|OVO.*|DANA.*)[\s]+([0-9,.]+)",y)
                if payment:
                    meta['method'] = "WALLET"
                    meta['sof_wallet'] = payment.groups()[0]
                    meta['payment'] = payment.groups()[1]
                else:
                    payment = re.search(r"(?i)(BCA CARD|MASTER|AMEX|JCB|AMEX|UNION|VISA)[\s]+([0-9,.]+)",y)
                    if payment:
                        meta['method'] = "CC"
                        meta['issuer'] = payment.groups()[0]
                        meta['payment'] = payment.groups()[1]
                        payment = re.search(r"(?i)(dbs [0-9a-zA-Z]+)[\s]+([0-9,.]+,[0-9,.]+)",y)
                        if payment:
                            meta['issuer'] = payment.groups()[0]+" "+meta['issuer']
                    else:
                        payment = re.search(r"(?i)([a-zA-Z0-9\%]+\sDEBIT.*|[a-zA-Z0-9\%]+\sDEBET.*|DEBET.*|DEBIT.*)[\s]+([0-9]+,[0-9]+)",y)
                        if payment:
                            meta['method'] = "DC"
                            meta['issuer'] = payment.groups()[0]
                            meta['payment'] = payment.groups()[1]
                        else:
                            # payment = re.search(r"(?i)(VISA)[\s]+([0-9,.]+)",y)
                            # if payment:
                            meta['method'] = "CASH"
                            meta['payment'] = ""
            change = re.search(r"(?i)(CHANGE)[\s]+([0-9,.]+)",y)
            if change:
                change = change.groups()[1]
            else:
                change = str("0")
            meta['change'] = int(float(change.replace(",","")))
            data['meta'] = meta
            if total: 
                total = total.groups()[0]
            else:
                total = str(subtotal + int(tax.replace(",","")) + service + delivery_service_charge - int(discount))
            data['subtotal'] = subtotal
            data['total'] = math.ceil(float(total.replace(",",""))/100)*100
            data['tax'] = int(float(tax.replace(",","")))
            data['receipt_date'] = date_time_obj

            # data = json.dumps(data, default=json_util.default)
            return data 
        else: 
            return False
        # print(res)
        # request_data = {
        #     'ID': data['id_trx'],
        #     'TANGGAL_TRX': data['date_trx'],
        #     'NO_STRUK': data['no_struk'],
        #     'DESCRIPTION': data['description'],
        #     'AMOUNT_TRX': str(data['amount']),
        #     'NOPD': str(data['no_pd']),
        #     'SOURCE': self.source
        # }

        # token = base64.b64encode(f'{self.username}:{self.password}'.encode())

        # headers = {
        #     'Accept': 'application/json',
        #     'Authorization': 'Basic {}'.format(token.decode('utf-8'))
        # }

        # try:
        #     current_app.logger.info(dict(
        #         message="=== Request BPRD ===",
        #         url=self.main_url,
        #         body=request_data,
        #         headers=headers
        #     ))

        #     r = requests.post(self.main_url, request_data, headers=headers)

        #     current_app.logger.info(dict(
        #         message="=== Response from BPRD ===",
        #         url=r.request.url,
        #         response=r.content,
        #         status_code=r.status_code
        #     ))

        #     res = r.json()

        # except RequestException as e:
        #     current_app.logger.error(dict(
        #         message="=== Request Exception Raised ===",
        #         errors=e
        #     ))
        #     raise
        # except(JSONDecodeError, KeyError) as e:
        # current_app.logger.error(dict(
        #     message="=== JSON not Valid ===",
        #     errors=e
        # ))
        # raise

