from flask import Blueprint, jsonify

status = Blueprint('status', __name__)

@status.route('/status', methods=['GET'])
def status_view():
    return {'status': 'ok'}, 200