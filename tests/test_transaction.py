import pytest

from app import db as _db
from app.transaction import transactions
from app.common.ninetynine import NinetyNine


@pytest.fixture(scope='module')
def product_map_seed(request, app):
    _db.app = app

    Product.query.delete()
    product = Product(
        shop_id=123,
        product_id=4001,
        acommerce_sku='ACOM_0124',
    )
    _db.session.add(product)
    _db.session.commit()

    def teardown():
        Product.query.delete()
        _db.session.commit()

    request.addfinalizer(teardown)


@pytest.mark.usefixtures('product_map_seed')
def test_setmap_new(client, monkeypatch):
    product_id = 123
    shop_id = 246
    acommerce_sku = 'ACOM_0123'

    product = Product.query.filter_by(product_id=product_id,
                                      shop_id=shop_id).first()
    assert product is None

    monkeypatch.setattr(views, 'parse_token', lambda: dict(
        shop_id=shop_id,
        client_id='myclient',
        client_secret='secret01'
    ))
    res = client.patch('/products/mapping', data=dict(
        channel_main_sku=str(product_id),
        channel_variant_sku=str(product_id),
        acommerce_sku=acommerce_sku
    ))
    assert res.status_code == 201
    product = Product.query.filter_by(product_id=product_id,
                                      shop_id=shop_id).first()
    assert product is not None
    assert product.product_id == product_id
    assert product.acommerce_sku == acommerce_sku

    new_acom_sku = 'ACOM_0125'
    res = client.patch('/products/mapping', data=dict(
        channel_main_sku=str(product_id),
        channel_variant_sku=str(product_id),
        acommerce_sku=new_acom_sku
    ))
    assert res.status_code == 200
    product = Product.query.filter_by(product_id=product_id,
                                      shop_id=shop_id).first()
    assert product is not None
    assert product.product_id == product_id
    assert product.acommerce_sku == new_acom_sku


# @pytest.mark.usefixtures('product_map_seed')
# def test_setmap_exist(client, monkeypatch):
#     product_id = 4001
#     new_product_id = 4005
#     shop_id = 123
#     acommerce_sku = 'ACOM_0124'
#
#     product = Product.query.filter_by(product_id=product_id,
#                                       shop_id=shop_id).first()
#     assert product is not None
#
#     monkeypatch.setattr(views, 'parse_token', lambda: dict(
#         shop_id=shop_id,
#         client_id='myclient',
#         client_secret='secret01'
#     ))
#     res = client.patch('/products/mapping', data=dict(
#         channel_main_sku=str(new_product_id),
#         channel_variant_sku=str(new_product_id),
#         acommerce_sku=acommerce_sku
#     ))
#     assert res.status_code == 200
#     product = Product.query.filter_by(product_id=product_id,
#                                       shop_id=shop_id).first()
#     assert product is not None
#     assert product.product_id == product_id
#     assert product.acommerce_sku == acommerce_sku
