import pytest

from app import create_app

@fixture
def global_fixture():
    print("\n(Doing global fixture setup stuff!)")
        
@pytest.fixture(scope='session')
def app(request):
    _app = create_app()

    ctx = _app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)

    return _app


@pytest.fixture(scope='function')
def client(app):
    """Flask test client fixture
    """
    client = app.test_client()

    yield client
